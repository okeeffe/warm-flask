from flask import jsonify, request
from flask_cors import CORS
import json

from app import db, socketio
from app.main import api_v1
from app.main.models import Poll, Poll_Question, \
    Poll_Response, Poll_Response_Answer
from utils.utils_datetime import tzware_datetime
from utils.utils_errors import NonExistentPoll, NonExistentResponse, \
    ResponseComplete, QuestionNotInPoll

CORS(api_v1)


@api_v1.route('/')
def index():
    """Would be the GET for a scoped list of polls (for the requesting user)
    and the POST point for a new poll."""
    return jsonify(msg='Welcome to the Warmflask API!')


@api_v1.route('/<poll_id>')
def get_poll(poll_id):
    """
    Returns the name and questions for a specific poll.
    Also returns the in-progress response if '?r_id' supplied.
    """
    return_json = {}

    poll = Poll.query.get(poll_id)
    try:
        if poll is None:
            raise NonExistentPoll('That poll does not exist!')

        return_json['poll'] = poll.as_safe_dict()

        response_id = request.args.get('r_id')
        if response_id is not None:
            poll_response = (Poll_Response.query.filter_by
                             (id=response_id).first())
            if poll_response is None:
                raise NonExistentResponse('That response does not exist!')

            return_json['response'] = poll_response.as_safe_dict()
            if 'completed_on' not in return_json['response']:
                last_answer = poll_response.answers[-1]
                last_q_answered = next(q for q in poll.questions
                                       if q.id == last_answer.question_id)
                # Need current_step zero-indexed for the front-end and to
                # conform with the dict/JSON structure.
                current_step = last_q_answered.step - 1
                last_q_step = return_json['poll']['steps'][current_step]
                # Check if last_q_answered was the last in its step.
                # If it was, we need to move on a step.
                if last_q_step[-1]['id'] == last_q_answered.id:
                    # Safe because otherwise poll_response completed_on
                    # would be set.
                    current_step += 1
                return_json['response']['current_step'] = current_step

        return jsonify(return_json)

    except NonExistentPoll as nep:
        error = {
            'poll_id': poll_id,
            'msg': nep.message,
            'status_code': 404
        }
        response = jsonify({'error': error})
        response.status_code = 404
        return response
    except NonExistentResponse as ner:
        error = {
            'poll_id': poll_id,
            'response_id': response_id,
            'msg': ner.message,
            'status_code': 404
        }
        response = jsonify({'error': error})
        response.status_code = 404
        return response


@api_v1.route('/<poll_id>/<response_id>', methods=['POST'])
def response_to_poll(poll_id, response_id):
    """Accepts answers for a specific poll"""
    # If no valid response_id given (front end sends -1), create a new one.
    # Could do this by adding a 'POST' method to the base get_poll and handling
    # it, but it felt unclean.
    poll_response = {}  # Placeholder variable.
    if response_id == '-1':
        poll_response = Poll_Response(poll_id=poll_id)

    try:
        # Otherwise, we need to load the existing response.
        if isinstance(poll_response, dict):
            poll_response = Poll_Response.query.get(response_id)
            if poll_response is None:
                raise NonExistentResponse('That response does not exist!')
            # A new response couldn't be completed yet, but an existing one
            # could be.
            elif poll_response.completed_on is not None:
                raise ResponseComplete('That response is complete.')

        data = request.get_json()
        # answers should be an array of dicts like {question_id: answer_value}
        answers = data['answers']
        for answer in answers:
            question_id = next(iter(answer))
            # Check that the question is in this poll
            question = Poll_Question.query.get(question_id)
            if not question or question.poll_id != int(poll_id):
                raise QuestionNotInPoll('This question is not in this poll!',
                                        question_id, data)

            value = answer[question_id]
            for poll_answer in poll_response.answers:
                if poll_answer.question_id == int(question_id):
                    # updating existing answer
                    poll_answer.value = value
                    break
            else:
                # a new answer
                ans = Poll_Response_Answer(response_id=poll_response.id,
                                           question_id=int(question_id),
                                           value=value)
                poll_response.answers.append(ans)

        if (len(poll_response.answers) ==
                len(Poll_Question.query.filter_by(poll_id=poll_id).all())):
            poll_response.completed_on = tzware_datetime()

        db.session.add(poll_response)
        db.session.commit()

        return_json = {'response': poll_response.as_safe_dict()}

        socketio.emit('new_response',
                      {'response': poll_response.as_safe_dict(admin=True)})

        return jsonify(return_json)

    except NonExistentResponse as ner:
        error = {
            'poll_id': poll_id,
            'response_id': response_id,
            'msg': ner.message,
            'status_code': 404
        }
        response = jsonify({'error': error})
        response.status_code = 404
        return response
    except ResponseComplete as rc:
        error = {
            'poll_id': poll_id,
            'response_id': response_id,
            'msg': rc.message,
            'status_code': 404
        }
        response = jsonify({'error': error})
        response.status_code = 403
        return response
    except QuestionNotInPoll as qnip:
        error = {
            'poll_id': poll_id,
            'response_id': response_id,
            'question_id': qnip.question_id,
            'msg': qnip.message,
            'received': qnip.data,
            'status_code': 404
        }
        response = jsonify({'error': error})
        response.status_code = 404
        return response
    except:
        error = {
            'poll_id': poll_id,
            'msg': 'This endpoint requires valid JSON.',
            'status_code': 400,
            'example': json.dumps({'answers':
                                  [{'q_id': 'answer_value'},
                                   {1: 'Tobias Funke'},
                                   {2: 'bluetobias@bluth.co'}]})
        }
        response = jsonify({'error': error})
        response.status_code = 400
        return response
