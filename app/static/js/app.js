var app = angular.module('app', []);

(function() {
  function flatten(arr) {
    var ret = [];
    for (var i = 0; i < arr.length; i++) {
      if (Array.isArray(arr[i])) {
        ret = ret.concat(flatten(arr[i]));
      } else {
        ret.push(arr[i]);
      }
    }
    return ret;
  }

  // Change the Angular interpolator symbols so they do not conflict with Jinja2 moustaches
  app.config(['$interpolateProvider', function($interpolateProvider) {
    $interpolateProvider.startSymbol('{a');
    $interpolateProvider.endSymbol('a}');
  }]);

  app.controller('mainController', ['$scope', '$timeout', '$http', function($scope, $timeout, $http) {
    RECEIVED_INITIAL_DATA = false;

    $scope.highlightLast = false;
    $scope.POLL = null
    $scope.responses = []

    $scope.getFormattedDate = function(timestamp) {
      var date = new Date(timestamp * 1000);

      var month = date.getMonth() + 1;
      var day = date.getDate();
      var hour = date.getHours();
      var min = date.getMinutes();
      var sec = date.getSeconds();

      month = (month < 10 ? "0" : "") + month;
      day = (day < 10 ? "0" : "") + day;
      hour = (hour < 10 ? "0" : "") + hour;
      min = (min < 10 ? "0" : "") + min;
      sec = (sec < 10 ? "0" : "") + sec;

      var str = date.getFullYear() + "-" + month + "-" + day + " " + hour + ":" + min + ":" + sec;

      return str;
    }

    var SOCKET = io.connect('http://' + document.domain + ':' + location.port);

    SOCKET.on('connect', function() {
      SOCKET.emit('connected', {
        data: 'I\'m connected!'
      });
    });

    SOCKET.on('poll_data', function(data) {
      if(!RECEIVED_INITIAL_DATA) {
        var obj = JSON.parse(data);
        $scope.$apply(function() {
          $scope.POLL = obj.poll;
          $scope.POLL.questions = flatten($scope.POLL.steps);
          $scope.responses = $scope.responses.concat(obj.responses);
        });
        RECEIVED_INITIAL_DATA = true;
      }
    });

    SOCKET.on('new_response', function(data) {
      var response = data.response;
      if (response) {
        var existingResponse = false;
        for (var rIndex = 0; rIndex < $scope.responses.length; rIndex++) {
          localResponse = $scope.responses[rIndex];
          if (localResponse.id === response.id) {
            existingResponse = true;
            $scope.$apply(function() {
              $scope.responses[rIndex] = response;
            });
            break;
          }
        }

        if (!existingResponse) {
          $scope.$apply(function() {
            $scope.responses = $scope.responses.concat(response);
            $scope.highlightLast = true;
            $timeout(function() {
              $scope.highlightLast = false;
            }, 3000);
          });
        }
      }
    });
  }]);
  app.directive('highlighter', ['$timeout', function($timeout) {
    return {
      restrict: 'A',
      scope: {
        model: '=highlighter'
      },
      link: function(scope, element) {
        scope.$watch('model', function(newVal, oldVal) {
          if (newVal !== oldVal) {
            // apply class
            element.addClass('highlight');

            // auto remove after some delay
            $timeout(function() {
              element.removeClass('highlight');
            }, 3000);
          }
        });
      }
    };
  }]);
})();
