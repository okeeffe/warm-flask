(function(globalObj) {
  "use strict";

  var DOC = globalObj.document;

  // Just in case console doesn't exist
  var method;
  var noop = function() {};
  var console = (globalObj.console = globalObj.console || {});
  if (!console['error']) {
    console['error'] = noop;
  }

  /***** DOM MANIPULATION *****/

  var Q_ID_ROOT = 'wf_q_';
  var Q_LABEL_ID_ROOT = 'wf_label_q_';
  var STEP_ID_ROOT = 'wf_step_';

  function requestPollCss() {
    if (DOC) {
      var cssLink = DOC.createElement('link');
      cssLink.href = '//warmflask.dev:5000/static/css/warmflask-polls.min.css';
      cssLink.rel = 'stylesheet';
      DOC.getElementsByTagName('head')[0].appendChild(cssLink);
    }
  }

  function buildLabel(question) {
    var label = DOC.createElement('label');
    label.id = Q_LABEL_ID_ROOT + question.id;
    label.classList.add('q-label');
    label.innerHTML = question.wording;
    label.htmlFor = Q_ID_ROOT + question.id;

    if (question.required) {
      label.innerHTML += '*';
    }

    return label;
  }

  function buildTextInputQuestion(question) {
    var questionEl = DOC.createElement('div');

    var label = buildLabel(question);

    var input = DOC.createElement('input');
    input.type = question.type;
    input.id = Q_ID_ROOT + question.id;
    if (question.required) {
      input.required = 'required';
    }

    if (api._response.answers && api._response.answers[question.id]) {
      input.value = api._response.answers[question.id].value;
    }

    questionEl.appendChild(label);
    questionEl.appendChild(input);

    return questionEl;
  }

  function buildSelectQuestion(question) {
    var questionEl = DOC.createElement('div');

    var label = buildLabel(question);

    var select = DOC.createElement('select');
    select.id = Q_ID_ROOT + question.id;
    if (question.required) {
      select.required = 'required';
    }

    var selectedValue = null;
    if (api._response.answers && api._response.answers[question.id]) {
      selectedValue = api._response.answers[question.id].value;
    }

    // Add "please select" option
    var optionEl = DOC.createElement('option');
    optionEl.text = "Please select";
    optionEl.value = '';
    if (!selectedValue) {
      optionEl.selected = true;
    }
    select.appendChild(optionEl);

    for (var optionCount = 0; optionCount < question.options.length; optionCount++) {
      var option = question.options[optionCount];
      var optionEl = DOC.createElement('option');
      optionEl.text = option.text;
      optionEl.value = option.value;
      if (optionEl.value === selectedValue) {
        optionEl.selected = true;
      }
      select.appendChild(optionEl);
    }

    questionEl.appendChild(label);
    questionEl.appendChild(select);

    return questionEl;
  }

  function buildFreeformQuestion(question) {
    var questionEl = DOC.createElement('div');

    var label = buildLabel(question);

    var input = DOC.createElement('textarea');
    input.id = Q_ID_ROOT + question.id;
    if (question.required) {
      input.required = 'required';
    }

    if (api._response.answers && api._response.answers[question.id]) {
      input.innerHTML = api._response.answers[question.id].value;
    }

    questionEl.appendChild(label);
    questionEl.appendChild(input);

    return questionEl;
  }

  function buildRadioOrCheckboxQuestion(question) {
    var questionEl = DOC.createElement('div');

    var label = buildLabel(question);

    var radioDiv = DOC.createElement('div');
    radioDiv.id = Q_ID_ROOT + question.id + '_div';

    var selectedValue = null;
    if (api._response.answers && api._response.answers[question.id]) {
      selectedValue = api._response.answers[question.id].value;
    }

    for (var i = 0; i < question.options.length; i++) {
      var option = question.options[i];
      var optionEl = DOC.createElement('input');
      optionEl.type = question.type;
      optionEl.name = Q_ID_ROOT + question.id;
      optionEl.id = Q_ID_ROOT + question.id + '_' + i;
      optionEl.value = option.value;
      if (optionEl.value === selectedValue) {
        optionEl.checked = true;
      }

      if (question.required) {
        optionEl.required = 'required';
      }

      var optionLabel = DOC.createElement('label');
      optionLabel.id = Q_LABEL_ID_ROOT + question.id;
      optionLabel.innerHTML = option.text;

      optionLabel.appendChild(optionEl);
      radioDiv.appendChild(optionLabel);
    }

    questionEl.appendChild(label);
    questionEl.appendChild(radioDiv);

    return questionEl;
  }

  var questionBuilders = {
    'text': buildTextInputQuestion,
    'email': buildTextInputQuestion,
    'tel': buildTextInputQuestion,
    'select': buildSelectQuestion,
    'freeform': buildFreeformQuestion,
    'radio': buildRadioOrCheckboxQuestion,
    'checkbox': buildRadioOrCheckboxQuestion
  };

  function buildQuestion(question) {
    return questionBuilders[question.type](question);
  }

  function hideShownStep() {
    var activeStep = DOC.querySelector('.step.active');
    if (activeStep) {
      activeStep.classList.remove('active');
    }
  }

  function showStep() {
    hideShownStep();

    if (api._poll.current_step === 0 || api._poll.current_step) {
      var step = DOC.getElementById(STEP_ID_ROOT + api._poll.current_step);
      step.classList.add('active');
    }
  }

  function clearStepErrors() {
    var errors = DOC.querySelectorAll('.q-label.error');
    for (var labelCount = 0; labelCount < errors.length; labelCount++) {
      var qLabel = errors[labelCount];
      qLabel.innerHTML = qLabel.getAttribute('data-old');
      qLabel.removeAttribute('data-old');
      qLabel.classList.remove('error');
    }
  }

  function nextBtnClick(e) {
    clearStepErrors();

    var stepResponse = {
      answers: []
    };
    var currStep = api._poll.current_step;
    var step = api._poll.steps[currStep];
    var stepErrors = {};

    for (var i = 0; i < step.length; i++) {
      var question = step[i];
      var qId = question.id;
      var answerObj = {};

      if (question.type === 'checkbox' || question.type === 'radio') {
        var checked = DOC.querySelectorAll('input[name=' + Q_ID_ROOT + qId + ']:checked');
        if (checked.length > 0) {
          var answers = [];
          for (var nodeCount = 0; nodeCount < checked.length; nodeCount++) {
            answers.push(checked[nodeCount].value);
          }
          answer = answers.join(', ');
          answerObj[qId] = answer;
          stepResponse.answers.push(answerObj)
        } else if (question.required) {
          // Give them an error
          stepErrors[qId] = question.wording + ' requires an answer!';
        } else {
          answerObj[qId] = '';
          stepResponse.answers.push(answerObj)
        }
      } else {
        var answer = DOC.getElementById(Q_ID_ROOT + qId).value;

        if (answer.length > 0) {
          answerObj[qId] = answer;
          stepResponse.answers.push(answerObj)
        } else if (question.required) {
          // Give them an error
          stepErrors[qId] = question.wording + ' requires an answer!';
        } else {
          answerObj[qId] = '';
          stepResponse.answers.push(answerObj)
        }
      }
    }

    var errKeys = Object.keys(stepErrors);
    if (errKeys.length > 0) {
      for (var keyCount = 0; keyCount < errKeys.length; keyCount++) {
        var qLabel = DOC.getElementById(Q_LABEL_ID_ROOT + errKeys[keyCount]);
        qLabel.setAttribute('data-old', qLabel.innerHTML);
        qLabel.innerHTML = stepErrors[errKeys[keyCount]];
        qLabel.classList.add('error');
      }
    } else {
      api.sendAnswers(stepResponse, function(data) {
        api._response = data.response;
        api._responseId = data.response.id;
        if (!localStorage.getItem('_wf_response_id')) {
          localStorage.setItem('_wf_response_id', data.response.id);
        }
      }, console.error);

      // Don't wait for successful post, just keep going
      if (currStep < api._poll.steps.length - 1) {
        api._poll.current_step++;
        showStep();
      } else {
        hideShownStep();
        DOC.getElementById('wf_thanks').classList.add('active');

        globalObj.setTimeout(function() {
          var poll = DOC.getElementById('wf_container');
          poll.parentNode.removeChild(poll);
        }, 3000);
      }
    }

  }

  function backBtnClick(e) {
    if (api._poll.current_step > 0) {
      api._poll.current_step--;
      showStep();
    }
  }

  function tabClick(e) {
    var pollEl = DOC.getElementById('wf_poll');
    var span = DOC.getElementById('wf_poll_tab_span');

    pollEl.classList.toggle('open');
    if (pollEl.classList.contains('open')) {
      span.innerHTML = "&#8722;"; // minus
      pollEl.onclick = noop;
    } else {
      span.innerHTML = "&#43;"; // plus
      pollEl.onclick = function(e) {
        pollEl.classList.add('open');
        span.innerHTML = "&#8722;";
      }
    }
  }

  function buildStep(step, num, numSteps) {
    var stepEl = DOC.createElement('div');
    stepEl.id = STEP_ID_ROOT + num;
    stepEl.classList.add('step');

    var questionsContainer = DOC.createElement('div');

    var header = DOC.createElement('h4');
    header.innerHTML = 'Step ' + step[0].step + ' of ' + numSteps;
    questionsContainer.appendChild(header);

    questionsContainer.classList.add('questions');
    for (var i = 0; i < step.length; i++) {
      questionsContainer.appendChild(buildQuestion(step[i]));
    }

    stepEl.appendChild(questionsContainer);

    var buttonsContainer = DOC.createElement('div');
    buttonsContainer.classList.add('wf-footer');
    buttonsContainer.classList.add('wf-clearfix');

    var nextBtn = DOC.createElement('button');
    nextBtn.type = 'button';
    nextBtn.id = STEP_ID_ROOT + num + '_next';
    nextBtn.classList.add('next');
    nextBtn.classList.add('primary');
    nextBtn.innerHTML = 'Next &gt;';
    if (num === numSteps - 1) {
      nextBtn.innerHTML = 'Done';
    }
    nextBtn.onclick = nextBtnClick;

    stepEl.addEventListener('keydown', function(e) {
      if ((e.which === 13 || e.keyCode === 13) && e.target.tagName.toUpperCase() !== 'TEXTAREA') {
        nextBtn.click();
      } else if (e.which === 27 || e.keyCode === 27) {
        DOC.getElementById('wf_poll_tab').click();
      }
    });

    if (num > 0) {
      var backBtn = DOC.createElement('button');
      backBtn.type = 'button';
      backBtn.id = STEP_ID_ROOT + num + '_back';
      backBtn.classList.add('back');
      backBtn.innerHTML = '&lt; Previous';
      buttonsContainer.appendChild(backBtn);
      backBtn.onclick = backBtnClick;
    }

    buttonsContainer.appendChild(nextBtn);
    stepEl.appendChild(buttonsContainer);

    return stepEl;
  }


  function buildPoll(pollData) {
    if (DOC) {
      var pollContainer = DOC.createElement('div');
      pollContainer.id = 'wf_container';
      pollContainer.classList.add('wf');
      pollContainer.style.display = 'none';

      var pollEl = DOC.createElement('div');
      pollEl.id = 'wf_poll';
      pollEl.classList.add('wf-poll');
      pollEl.classList.add('open');

      var tab = DOC.createElement('div');
      tab.id = 'wf_poll_tab';
      tab.classList.add('tab');
      var span = DOC.createElement('span');
      span.id = 'wf_poll_tab_span';
      span.innerHTML = "&#8722;"
      tab.onclick = tabClick;
      tab.appendChild(span);
      pollContainer.appendChild(tab);

      for (var i = 0; i < pollData.steps.length; i++) {
        pollEl.appendChild(buildStep(pollData.steps[i], i, pollData.steps.length));
      }

      var thanksEl = DOC.createElement('div');
      thanksEl.id = 'wf_thanks';
      thanksEl.classList.add('step');
      thanksEl.classList.add('thanks');
      var thanksMsg = DOC.createElement('h3');
      thanksMsg.innerHTML = 'Thank you for your time!';
      thanksEl.appendChild(thanksMsg);

      pollEl.appendChild(thanksEl);
      pollContainer.appendChild(pollEl);
      DOC.getElementsByTagName('body')[0].appendChild(pollContainer);

      globalObj.setTimeout(function() {
        showStep();
        pollContainer.style.display = 'block';
      }, 2000);
    }
  }


  /***** THE API *****/

  var api = {};
  api._url = "//warmflask.dev:5000/api/v1";

  // Below two vars could be filled in server-side
  api._userId = globalObj._wfSettings.wfid;
  api._pollId = 1; // Could be an object like { pageRegex: pollId }

  api._responseId = localStorage.getItem('_wf_response_id') || -1;
  api._poll = null; // Could be an object like { pollId: poll }
  api._response = {
    current_step: 0
  }; // Could be an object like { pollId: response }

  api.validFunctions = [
    'getPoll',
    'sendAnswers',
    'buildPoll',
    'init'
  ];

  api.buildPoll = function() {
    if (api._poll) {
      requestPollCss();
      buildPoll(api._poll);
    } else {
      console.error('Tried to build a nonexistent poll');
    }
  }

  api.getPoll = function(res, err) {
    var url = api._url + '/' + api._pollId + (api._responseId > 0 ? '?r_id=' + api._responseId : '');
    return getJSON(url, res, err);
  };

  function handlePoll(data) {
    if (data.poll) {
      api._poll = data.poll;
      if (data.response) {
        api._response = data.response;
        api._responseId = data.response.id;
      }

      if (api._response.current_step === 0 || api._response.current_step) {
        api._poll.current_step = api._response.current_step;
      }

      if (!data.response || !data.response.completed_on) {
        api.buildPoll();
      }
    }
  }

  api.init = function() {
    api.getPoll(handlePoll, console.error);
  }

  api.sendAnswers = function(data, res, err) {
    var url = api._url + '/' + api._pollId + '/' + api._responseId;
    return post(url, data, res, err);
  };


  /***** /THE API *****/

  /***** XHR HELPERS *****/

  var _networkError = '{"error":{"status_code":502,"type":"network_error","msg":"Warmflask: Could not communicate with server. Please try again in a few moments."}}';
  var _timeoutError = '{"error":{"status_code":408,"type":"timeout_error","msg":"Warmflask: Request timed out while trying to communicate with server. Please try again in a few moments."}}';

  function createCORSRequest(verb, url) {
    var xhr = new XMLHttpRequest();
    if ("withCredentials" in xhr) {
      // Check if the XMLHttpRequest object has a "withCredentials" property.
      // "withCredentials" only exists on XMLHTTPRequest2 objects.
      xhr.open(verb, url, true);
    } else if (typeof XDomainRequest !== "undefined") {
      // Otherwise, check for XDomainRequest.
      // XDomainRequest only exists in IE, and is IE's way of making CORS requests.
      // Note that these don't work with basic auth, so all they could do is authenticate.
      xhr = new XDomainRequest();
      xhr.open(verb, url);
    } else if (typeof module !== 'undefined' && typeof module.exports !== 'undefined' ||
      typeof define === 'function' && define.amd) {
      // Mocha unit testing
      xhr.open(verb, url, true);
    } else {
      // Otherwise, CORS is not supported by the browser.
      xhr = null;
    }
    return xhr;
  }

  function getJSON(url, res, err) {
    return get(url, function(recData) {
      res(JSON.parse(recData));
    }, function(errData) {
      err(JSON.parse(errData));
    });
  }

  var _oneSecond = 1000,
    _thirtySeconds = _oneSecond * 30;

  function get(url, res, err) {
    var req = createCORSRequest("GET", url);
    if (!req) {
      err(Error('CORS not supported.'));
      return;
    }

    req.timeout = _thirtySeconds;

    req.onload = function() {
      if (req.status >= 200 && req.status <= 300) {
        return res(req.response);
      } else {
        return err(req.response);
      }
    };

    // Handle network errors
    req.onerror = function() {
      if (err && err !== undefined) {
        return err(_networkError);
      }
    };

    // Handle timeout error
    req.ontimeout = function() {
      if (err && err !== undefined) {
        return err(_timeoutError);
      }
    };

    // Make the request
    req.send();

    return req;
  }

  function post(url, data, res, err) {
    return sendDataXhr(url, "POST", data, function(recData) {
      if (recData) {
        res(JSON.parse(recData));
      } else {
        res();
      }
    }, function(errData) {
      err(JSON.parse(errData));
    });
  }

  function sendDataXhr(url, verb, data, res, err) {
    var req = createCORSRequest(verb.toUpperCase(), url);
    if (!req) {
      return err(Error('CORS not supported.'));
    }

    req.timeout = _thirtySeconds;

    // Send the proper header information along with the request
    req.setRequestHeader("Content-type", "application/json");

    req.onload = function() {
      if (req.status >= 200 && req.status <= 300) {
        return res(req.response);
      } else {
        return err(req.response);
      }
    };

    // Handle network errors
    req.onerror = function() {
      if (err && err !== undefined) {
        return err(_networkError);
      }
    };

    // Handle timeout error
    req.ontimeout = function() {
      if (err && err !== undefined) {
        return err(_timeoutError);
      }
    };

    // Make the request
    req.send(JSON.stringify(data));
    return req;
  }

  /***** /XHR HELPERS *****/

  if (!globalObj._wf) {
    globalObj._wf = api;
  }
})(window);

window._wf.init();
