from sqlalchemy_utils import database_exists, create_database

from app import create_app, db
from app.main import models
from utils.utils_datetime import tzware_datetime


def setup_db():
    app = create_app()
    with app.app_context():
        create_and_seed_db(app)


def create_and_seed_db(app):
    db_uri = app.config['SQLALCHEMY_DATABASE_URI']
    if not database_exists(db_uri):
        create_database(db_uri)
    db.create_all()
    seed_db()


def teardown_db():
    app = create_app()
    with app.app_context():
        destroy_db()


def destroy_db():
    db.session.remove()
    db.drop_all()


def seed_db():
    poll = models.Poll(name='The Poll')

    # Step 4 (added first to ensure step ordering works)
    q7 = models.Poll_Question(wording='Favourite book',
                              type='text',
                              required=False,
                              step=4)
    q8 = models.Poll_Question(wording='Favourite colors',
                              type='checkbox',
                              required=False,
                              step=4)
    colors = ['Red', 'Orange', 'Yellow', 'Green', 'Blue', 'Indigo', 'Violet']
    q8.options = [models.Poll_Question_Option(text=color,
                                              value=color.lower())
                  for color in colors]

    # Step 1
    q1 = models.Poll_Question(wording='Name',
                              type='text',
                              required=True,
                              step=1)
    q2 = models.Poll_Question(wording='Email',
                              type='email',
                              required=True,
                              step=1)

    # Step 2
    q3 = models.Poll_Question(wording='Age',
                              type='select',
                              required=True,
                              step=2)
    # Would be much more storage-efficient to support a range flag
    # and then just say range(0, 121) and let the front end iterate.
    q3.options = [models.Poll_Question_Option(text=str(i), value=str(i))
                  for i in xrange(121)]
    q4 = models.Poll_Question(wording='About me',
                              type='freeform',
                              required=True,
                              step=2)

    # Step 3
    q5 = models.Poll_Question(wording='Address',
                              type='freeform',
                              required=False,
                              step=3)
    q6 = models.Poll_Question(wording='Gender',
                              type='radio',
                              required=False,
                              step=3)
    q6.options = [models.Poll_Question_Option(text=gender,
                                              value=gender.lower())
                  for gender in ['Male', 'Female', 'Non-binary']]

    poll.questions.append(q1)
    poll.questions.append(q2)
    poll.questions.append(q3)
    poll.questions.append(q4)
    poll.questions.append(q5)
    poll.questions.append(q6)
    poll.questions.append(q7)
    poll.questions.append(q8)

    db.session.add(poll)
    db.session.commit()

    complete_response = models.Poll_Response(poll_id=poll.id,
                                             completed_on=tzware_datetime())

    db.session.add(complete_response)
    db.session.commit()

    q1a = models.Poll_Response_Answer(response_id=complete_response.id,
                                      question_id=q1.id,
                                      value='Gob Bluth')
    q2a = models.Poll_Response_Answer(response_id=complete_response.id,
                                      question_id=q2.id,
                                      value='gob@bluthcompany.com')
    q3a = models.Poll_Response_Answer(response_id=complete_response.id,
                                      question_id=q3.id,
                                      value='42')
    q4a = models.Poll_Response_Answer(response_id=complete_response.id,
                                      question_id=q4.id,
                                      value='A magician')
    q5a = models.Poll_Response_Answer(response_id=complete_response.id,
                                      question_id=q5.id,
                                      value='Bluth Estate')
    q6a = models.Poll_Response_Answer(response_id=complete_response.id,
                                      question_id=q6.id,
                                      value='male')
    q7a = models.Poll_Response_Answer(response_id=complete_response.id,
                                      question_id=q7.id,
                                      value='The Magic Circle')
    q8a = models.Poll_Response_Answer(response_id=complete_response.id,
                                      question_id=q8.id,
                                      value='red, indigo')

    complete_response.answers.append(q1a)
    complete_response.answers.append(q2a)
    complete_response.answers.append(q3a)
    complete_response.answers.append(q4a)
    complete_response.answers.append(q5a)
    complete_response.answers.append(q6a)
    complete_response.answers.append(q7a)
    complete_response.answers.append(q8a)

    db.session.add(complete_response)
    db.session.commit()

    incomplete_response = models.Poll_Response(poll_id=poll.id)

    db.session.add(incomplete_response)
    db.session.commit()

    q1ai = models.Poll_Response_Answer(response_id=incomplete_response.id,
                                       question_id=q1.id,
                                       value='Buster Bluth')
    q2ai = models.Poll_Response_Answer(response_id=incomplete_response.id,
                                       question_id=q2.id,
                                       value='buster@bluthcompany.com')
    q3ai = models.Poll_Response_Answer(response_id=incomplete_response.id,
                                       question_id=q3.id,
                                       value='35')
    q4ai = models.Poll_Response_Answer(response_id=incomplete_response.id,
                                       question_id=q4.id,
                                       value=('Hey, brother'))
    q5ai = models.Poll_Response_Answer(response_id=incomplete_response.id,
                                       question_id=q5.id,
                                       value='Bluth Estate')
    q6ai = models.Poll_Response_Answer(response_id=incomplete_response.id,
                                       question_id=q6.id,
                                       value='male')
    q7ai = models.Poll_Response_Answer(response_id=incomplete_response.id,
                                       question_id=q7.id,
                                       value='Costumes Weekly')

    incomplete_response.answers.append(q1ai)
    incomplete_response.answers.append(q2ai)
    incomplete_response.answers.append(q3ai)
    incomplete_response.answers.append(q4ai)
    incomplete_response.answers.append(q5ai)
    incomplete_response.answers.append(q6ai)
    incomplete_response.answers.append(q7ai)

    db.session.add(incomplete_response)
    db.session.commit()
