# Warm Flask Poller

Hello there, and welcome to this homage to Hotjar's polls.

## To Run

### Using virtualenv

Note that you must have postgresql installed locally for this method to work.

1. `cd` to the cloned directory
2. Setup a config.py in the root (follow config.example.py example)
3. `virtualenv venv`
4. `. venv/bin/activate`
5. `pip install -r requirements.txt`
6. `python run.py -c` to setup the DB and seed it with some example data
6. `python run.py` or `python tests.py` for the unit tests
7. (for test site on "different" domain, `npm -g install local web-server`, then `cd test-site;ws --no-cache --spa index.html` -- note that this points to a specific location dependent on where your "back-end" is serving from, and so do `warmflask.js` and `warmflask.min.js` - you can facilitate this using your `hosts` file)

### Using Docker & Docker Compose

1. `cd` to the cloned directory
2. Setup a config.py in the root (follow config.example.py example)
3. `docker-compose up --build` (and wait for five minutes)
4. In a new terminal tab, `docker-compose exec website python run.py -c` to setup the DB and seed it with some example data
4. Connect at your Docker IP
5. (for test site on "different" domain, `npm -g install local web-server`, then `cd test-site;ws --no-cache --spa index.html` -- note that this points to a specific location dependent on where your "back-end" is serving from, and so do `app/static/js/warmflask.js` and `app/static/js/warmflask.min.js` - you can facilitate this using your `hosts` file)
