import sys

from app import create_app, socketio
from utils.utils_db import setup_db, teardown_db

app = create_app()

if __name__ == '__main__':
    if '-c' in sys.argv:
        setup_db()
    elif '-d' in sys.argv:
        teardown_db()
    else:
        socketio.run(app)
