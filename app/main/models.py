from sqlalchemy import UniqueConstraint
import operator

from utils.utils_sqlalchemy import ResourceMixin, AwareDateTime
from utils.utils_datetime import tz_dt_as_epoch
from app import db


class Poll(ResourceMixin, db.Model):
    __tablename__ = 'polls'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255))
    questions = db.relationship('Poll_Question', backref='polls')
    responses = db.relationship('Poll_Response', backref='polls',
                                lazy='dynamic')
    # Would have a user_id foreign key column and a target page
    # if this was to be expanded.

    def __init__(self, **kwargs):
        # Call Flask-SQLAlchemy's handy constructor.
        super(Poll, self).__init__(**kwargs)

    def as_safe_dict(self):
        data = self.as_dict()
        data['created_on'] = tz_dt_as_epoch(data['created_on'])
        data['updated_on'] = tz_dt_as_epoch(data['updated_on'])
        qs_by_step = {}
        for q in self.questions:
            if q.step not in qs_by_step:
                qs_by_step[q.step] = [q.as_safe_dict()]
            else:
                qs_by_step[q.step].append(q.as_safe_dict())
        # Sorting ensures steps come back in order (by step num).
        data['steps'] = [v for (k, v) in sorted(qs_by_step.items(),
                                                key=operator.itemgetter(0))]
        return data


class Poll_Question(ResourceMixin, db.Model):
    """Potentially many of these per poll."""
    __tablename__ = 'poll_questions'

    id = db.Column(db.Integer, primary_key=True)
    poll_id = db.Column(db.Integer, db.ForeignKey('polls.id'))
    wording = db.Column(db.Text, nullable=False)
    type = db.Column(db.String(50), default='text', nullable=False)
    required = db.Column(db.Boolean, default=False)
    # Could have made a poll_steps table but this way is less complex.
    step = db.Column(db.Integer, default=0)
    options = db.relationship('Poll_Question_Option', backref='poll_questions')
    answers = db.relationship('Poll_Response_Answer', backref='poll_questions',
                              lazy='dynamic')

    def __init__(self, **kwargs):
        super(Poll_Question, self).__init__(**kwargs)

    def as_safe_dict(self):
        data = self.as_dict()
        del data['poll_id']
        del data['created_on']
        del data['updated_on']
        data['options'] = [option.as_safe_dict() for option in self.options]
        return data


class Poll_Question_Option(ResourceMixin, db.Model):
    """Many of these or none, for each question, depending on the question."""
    __tablename__ = 'poll_question_options'
    # Make sure we don't have two identical texts or values for a q option.
    __table_args__ = (UniqueConstraint('text', 'question_id'),
                      UniqueConstraint('value', 'question_id'))

    id = db.Column(db.Integer, primary_key=True)
    question_id = db.Column(db.Integer, db.ForeignKey('poll_questions.id'))
    text = db.Column(db.String(50), nullable=False)
    value = db.Column(db.String(50), nullable=False)

    def __init__(self, **kwargs):
        super(Poll_Question_Option, self).__init__(**kwargs)

    def as_safe_dict(self):
        data = self.as_dict()
        del data['id']
        del data['question_id']
        del data['created_on']
        del data['updated_on']
        return data


class Poll_Response(ResourceMixin, db.Model):
    """Only poll_responses rather than respondents + responses because
    the responses are pseudo anonymous, as with Hotjar, despite asking
    for a name in the example poll."""
    __tablename__ = 'poll_responses'

    # Could be a UUID but for simplicity just using an int.
    # This will get stored client-side to identify the respondent for
    # resuming an incomplete survey/knowing they're done.
    id = db.Column(db.Integer, primary_key=True)
    poll_id = db.Column(db.Integer, db.ForeignKey('polls.id'))
    completed_on = db.Column(AwareDateTime())
    answers = db.relationship('Poll_Response_Answer', backref='poll_responses')

    def __init__(self, **kwargs):
        super(Poll_Response, self).__init__(**kwargs)

    def as_safe_dict(self, admin=False):
        data = self.as_dict()
        data['created_on'] = tz_dt_as_epoch(data['created_on'])
        data['updated_on'] = tz_dt_as_epoch(data['updated_on'])
        if self.completed_on:
            data['completed_on'] = tz_dt_as_epoch(data['completed_on'])

        if not self.completed_on or admin:
            # Answers as a dict for easy referencing on the JS side.
            data['answers'] = {answer.question_id:
                               answer.as_safe_dict(admin=admin) for
                               answer in self.answers}

        if not self.completed_on:
            del data['completed_on']
        if admin:
            del data['created_on']
        return data


class Poll_Response_Answer(ResourceMixin, db.Model):
    """One of these per question answered - note answer could be null."""
    __tablename__ = 'poll_response_answers'

    id = db.Column(db.Integer, primary_key=True)
    response_id = db.Column(db.Integer, db.ForeignKey('poll_responses.id'))
    question_id = db.Column(db.Integer, db.ForeignKey('poll_questions.id'))
    # Currently not matching/checking these against a specific option,
    # but could do.
    # Treating the answer as a plaintext string no matter the type of question.
    value = db.Column(db.Text)

    def __init__(self, **kwargs):
        super(Poll_Response_Answer, self).__init__(**kwargs)

    def as_safe_dict(self, admin=False):
        data = self.as_dict()
        if admin:
            del data['created_on']
            del data['updated_on']
        else:
            data['created_on'] = tz_dt_as_epoch(data['created_on'])
            data['updated_on'] = tz_dt_as_epoch(data['updated_on'])
        del data['response_id']
        del data['id']
        return data
