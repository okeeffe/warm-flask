from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_socketio import SocketIO

db = SQLAlchemy()
socketio = SocketIO()


def create_app():
    """Create an application."""
    app = Flask(__name__)

    # Dev config
    app.config.from_object('config')
    # Test config
    app.config.from_envvar('WARM-FLASK_TEST_CONFIG', silent=True)

    db.init_app(app)
    socketio.init_app(app)

    from .main import admin as admin_blueprint
    app.register_blueprint(admin_blueprint)

    from .main import api_v1 as api_v1_blueprint
    app.register_blueprint(api_v1_blueprint, url_prefix='/api/v1')

    return app
