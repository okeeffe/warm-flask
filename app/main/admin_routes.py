from flask import current_app as c_app, send_from_directory, render_template, \
    session
from flask_socketio import emit, join_room
import json

from app import socketio
from app.main import admin
from app.main.models import Poll, Poll_Response
# import re


@admin.route('/')
def index():
    poll_id = 1
    session['poll_id'] = poll_id
    return render_template('admin.html',
                           poll_id=poll_id)


@socketio.on('connected')
def handle_connection(received):
    """Sent by clients when they connect to admin dashboard.
    Sends the poll info back to the client."""
    poll_id = session.get('poll_id')
    join_room(poll_id)

    poll = Poll.query.get(poll_id)
    poll_responses = (Poll_Response.query
                      .filter_by(poll_id=poll.id)
                      .all())
    poll_responses = [response.as_safe_dict(admin=True)
                      for response in poll_responses]
    poll = poll.as_safe_dict()

    response_json = {
        'poll': poll,
        'responses': poll_responses
    }

    emit('poll_data', json.dumps(response_json))


@admin.route('/s/<script_name>')
def serve_script(script_name):
    """Serve the user's site JS script.
    Would ideally be checking things based on the user ID and serving from
    a CDN.
    NO LONGER IN USE, JUST HERE AS A NOTE.
    """
    # try:
    #     user_id = re.search('warmflask\-(\d+)\.js', script_name).group(1)
    # except AttributeError:
    #     print('No user_id found')
    return send_from_directory(c_app.static_folder, 'js/warmflask.js',
                               add_etags=False, cache_timeout=1)
