class NonExistentResponse(Exception):
    """Error for when response does not exist in DB."""
    pass


class NonExistentPoll(Exception):
    """Error for when poll does not exist in DB."""
    pass


class ResponseComplete(Exception):
    """Error for when response is already complete but an attempt is made
    to update it."""
    pass


class QuestionNotInPoll(Exception):
    """Error for when response contains a question ID not in the
    referenced poll."""
    def __init__(self, message, question_id, data):
        super(Exception, self).__init__(message)
        self.question_id = question_id
        self.data = data
