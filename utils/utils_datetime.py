from datetime import datetime
from calendar import timegm
import pytz


def tzware_datetime():
    """
    Return a timezone aware datetime.

    :return: Datetime
    """
    return datetime.now(pytz.utc)


def tz_dt_as_epoch(tzdatetime):
    # ts = (tzdatetime - datetime(1970, 1, 1, tzinfo=pytz.utc)).total_seconds()
    ts = timegm(tzdatetime.timetuple())
    return ts


def tz_dt_str_to_datetime(tz_dt_str):
    return datetime.strptime(tz_dt_str,
                             "%a, %m %b %Y %H:%M:%S %Z")
