from flask import Blueprint

api_v1 = Blueprint('api_v1', __name__)
admin = Blueprint('admin', __name__)

from . import api_v1_routes, admin_routes, models
