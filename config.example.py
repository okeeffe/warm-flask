# dev configuration variables

DEBUG = True
SECRET_KEY = 'ISITSECRETISITSAFE'
CONFIRMATION_SALT = 'DEADSEA'

SQLALCHEMY_DATABASE_URI = ("postgresql://warmflasker:hotjar"
                           "@postgres:5432/warm-flask")
SQLALCHEMY_TRACK_MODIFICATIONS = False
