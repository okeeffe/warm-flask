import json
import os
import unittest

from app import create_app
from utils.utils_db import create_and_seed_db, destroy_db

os.environ['WARM-FLASK_TEST_CONFIG'] = '../config-test.py'
wf = create_app()
del os.environ['WARM-FLASK_TEST_CONFIG']


class BaseTestCase(unittest.TestCase):
    def setUp(self):
        self.app = wf.test_client()
        self.request_context = wf.test_request_context()
        self.premade_poll_id = 1
        self.premade_completed_response_id = 1
        self.premade_incomplete_response_id = 2
        self.no_response_id = -1

        with self.request_context:
            create_and_seed_db(wf)

    def tearDown(self):
        with self.request_context:
            destroy_db()
        pass


class AdminTestCase(BaseTestCase):
    def test_index(self):
        response = self.app.get('/', content_type='html/text')
        self.assertEqual(response.status_code, 200)
        self.assertIn('WarmflaskAdmin', response.data)


class APIGetPollTestCase(BaseTestCase):
    def test_index(self):
        response = self.app.get('/api/v1/')
        self.assertEqual(response.status_code, 200)
        json_data = json.loads(response.data)
        self.assertEqual(json_data['msg'], 'Welcome to the Warmflask API!')

    def test_get_poll_that_exists_no_response_id(self):
        response = self.app.get('/api/v1/{}'.format(self.premade_poll_id))
        self.assertEqual(response.status_code, 200)
        json_data = json.loads(response.data)
        self.assertEqual(json_data['poll']['name'], 'The Poll')
        self.assertEqual(json_data['poll']['id'], self.premade_poll_id)
        # Test steps are ordered
        self.assertEqual(json_data['poll']['steps'][0][0]['step'], 1)
        self.assertEqual(json_data['poll']['steps'][1][0]['step'], 2)
        self.assertEqual(json_data['poll']['steps'][2][0]['step'], 3)
        self.assertEqual(json_data['poll']['steps'][3][0]['step'], 4)
        # Check no response returned
        assert('response' not in json_data)

    def test_get_poll_that_exists_with_response_id_that_is_complete(self):
        response = self.app.get(('/api/v1/{}?r_id={}'
                                 .format(self.premade_poll_id,
                                         self.premade_completed_response_id)))
        self.assertEqual(response.status_code, 200)
        json_data = json.loads(response.data)
        self.assertEqual(json_data['poll']['name'], 'The Poll')
        self.assertEqual(json_data['poll']['id'], self.premade_poll_id)
        self.assertEqual(json_data['response']['id'],
                         self.premade_completed_response_id)
        assert('completed_on' in json_data['response'])
        assert('answers' not in json_data['response'])

    def test_get_poll_that_exists_with_response_id_that_is_incomplete(self):
        response = self.app.get(('/api/v1/{}?r_id={}'
                                 .format(self.premade_poll_id,
                                         self.premade_incomplete_response_id)))
        self.assertEqual(response.status_code, 200)
        json_data = json.loads(response.data)
        self.assertEqual(json_data['poll']['name'], 'The Poll')
        self.assertEqual(json_data['poll']['id'], self.premade_poll_id)
        self.assertEqual(json_data['response']['id'],
                         self.premade_incomplete_response_id)
        self.assertEqual(len(json_data['response']['answers']), 7)
        assert('completed_on' not in json_data['response'])
        self.assertEqual(json_data['response']['current_step'], 3)

    def test_get_poll_that_exists_with_response_id_that_doesnt_exist(self):
        response = self.app.get(('/api/v1/{}?r_id=-2'
                                 .format(self.premade_poll_id)))
        self.assertEqual(response.status_code, 404)
        json_data = json.loads(response.data)
        self.assertEqual(json_data['error']['msg'],
                         'That response does not exist!')

    def test_get_poll_that_doesnt_exist(self):
        response = self.app.get('/api/v1/{}'.format(self.premade_poll_id + 1))
        self.assertEqual(response.status_code, 404)
        json_data = json.loads(response.data)
        self.assertEqual(json_data['error']['msg'],
                         'That poll does not exist!')


class APIPostResponseTestCase(BaseTestCase):
    def setUp(self):
        BaseTestCase.setUp(self)

        with self.request_context:
            # Get the Poll
            response = self.app.get('/api/v1/{}'.format(self.premade_poll_id))
            self.assertEqual(response.status_code, 200)
            self.poll_json = json.loads(response.data)

    def test_post_response_nonexistent_response_id(self):
        q1_id = self.poll_json['poll']['steps'][0][0]['id']

        response = self.app.post('/api/v1/{}/-2'.format(self.premade_poll_id),
                                 data=json.dumps({'answers':
                                                 [{q1_id: 'Tobias Funke'}]}),
                                 content_type='application/json')
        self.assertEqual(response.status_code, 404)
        json_data = json.loads(response.data)
        self.assertEqual(json_data['error']['msg'],
                         'That response does not exist!')

    def test_post_response_no_response_id(self):
        q1_id = self.poll_json['poll']['steps'][0][0]['id']

        response = self.app.post(('/api/v1/{}/{}'
                                 .format(self.premade_poll_id,
                                         self.no_response_id)),
                                 data=json.dumps({'answers':
                                                 [{q1_id: 'Tobias Funke'}]}),
                                 content_type='application/json')
        self.assertEqual(response.status_code, 200)
        json_data = json.loads(response.data)
        self.assertEqual(json_data['response']['id'],
                         self.premade_incomplete_response_id + 1)
        self.assertEqual(json_data['response']['poll_id'],
                         self.premade_poll_id)
        self.assertEqual(len(json_data['response']['answers']), 1)

    def test_post_response_question_not_in_poll(self):
        response = self.app.post(('/api/v1/{}/{}')
                                 .format(self.premade_poll_id,
                                         self.premade_incomplete_response_id),
                                 data=json.dumps({'answers':
                                                 [{-1: 'Tobias Funke'}]}),
                                 content_type='application/json')
        self.assertEqual(response.status_code, 404)
        json_data = json.loads(response.data)
        self.assertEqual(json_data['error']['msg'],
                         'This question is not in this poll!')

    def test_post_response_with_json(self):
        q1_id = self.poll_json['poll']['steps'][0][0]['id']

        response = self.app.post(('/api/v1/{}/{}')
                                 .format(self.premade_poll_id,
                                 self.premade_incomplete_response_id),
                                 data=json.dumps({'answers':
                                                 [{q1_id: 'Tobias Funke'}]}),
                                 content_type='application/json')
        self.assertEqual(response.status_code, 200)
        json_data = json.loads(response.data)
        self.assertEqual(json_data['response']['id'],
                         self.premade_incomplete_response_id)
        ret_answers = json_data['response']['answers']
        self.assertEqual(len(ret_answers), 7)
        self.assertEqual(ret_answers[str(q1_id)]['question_id'], q1_id)
        self.assertEqual(ret_answers[str(q1_id)]['value'], 'Tobias Funke')

    def test_post_response_is_already_complete(self):
        q1_id = self.poll_json['poll']['steps'][0][0]['id']
        response = self.app.post(('/api/v1/{}/{}')
                                 .format(self.premade_poll_id,
                                         self.premade_completed_response_id),
                                 data=json.dumps({'answers':
                                                 [{q1_id: 'Tobias Funke'}]}),
                                 content_type='application/json')
        self.assertEqual(response.status_code, 403)
        json_data = json.loads(response.data)
        self.assertEqual(json_data['error']['msg'],
                         'That response is complete.')

    def test_post_existing_response_that_updates(self):
        response = self.app.get(('/api/v1/{}?r_id={}')
                                .format(self.premade_poll_id,
                                        self.premade_incomplete_response_id))
        poll_json = json.loads(response.data)
        q1_id = poll_json['poll']['steps'][0][0]['id']
        q1_ans = poll_json['response']['answers'][str(q1_id)]['value']
        response = self.app.post(('/api/v1/{}/{}')
                                 .format(self.premade_poll_id,
                                         self.premade_incomplete_response_id),
                                 data=json.dumps({'answers':
                                                 [{q1_id: 'Byron Bluth'}]}),
                                 content_type='application/json')
        self.assertEqual(response.status_code, 200)
        json_data = json.loads(response.data)
        assert('completed_on' not in json_data['response'])
        self.assertNotEqual(json_data['response']['answers']
                            [str(q1_id)]['value'],
                            q1_ans)
        self.assertEqual(json_data['response']['answers'][str(q1_id)]['value'],
                         'Byron Bluth')

    def test_post_existing_response_that_completes(self):
        q8_id = self.poll_json['poll']['steps'][3][1]['id']
        response = self.app.post(('/api/v1/{}/{}')
                                 .format(self.premade_poll_id,
                                         self.premade_incomplete_response_id),
                                 data=json.dumps({'answers':
                                                 [{q8_id: 'yellow, orange'}]}),
                                 content_type='application/json')
        self.assertEqual(response.status_code, 200)
        json_data = json.loads(response.data)
        assert('completed_on' in json_data['response'])
        assert('answers' not in json_data['response'])

    def test_post_response_without_json(self):
        response = self.app.post(('/api/v1/{}/{}')
                                 .format(self.premade_poll_id,
                                         self.premade_incomplete_response_id),
                                 data='Testing with bad JSON',
                                 content_type='application/json')
        self.assertEqual(response.status_code, 400)
        json_data = json.loads(response.data)
        self.assertEqual(json_data['error']['msg'],
                         'This endpoint requires valid JSON.')


if __name__ == '__main__':
    unittest.main()
